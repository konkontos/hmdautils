# HMDAUtils

An evolving collection of extensions and utilities for common tasks on os platforms.

Documentation: currently n/a
Test coverage: in progress

Currently working towards a stable v1.0.

![Swift 5.x](https://img.shields.io/badge/Swift-5-orange.svg?style=flat)
![iOS](https://img.shields.io/badge/platform-ios-lightgrey.svg?style=flat)
![macOS](https://img.shields.io/badge/platform-macos-lightgrey.svg?style=flat)

